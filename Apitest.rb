#!/usr/bin/env ruby
require 'configatron'
require 'rest-client'
require 'twitch/chat'
require_relative 'config.rb'
require 'json'

#some golbal vars to make things easier
$timepull = nil
$timenow = nil
$user1

def follower_new
	follows = RestClient.get "https://api.twitch.tv/helix/users/follows?to_id=#{configatron.chanid}", {:'Client-ID' => configatron.clientid}
	
	follows1 = JSON.parse(follows).fetch('data').first.each_with_index { |val| }
	userid = follows1.values_at('from_id')
	
	userid = userid.to_s.delete('[{}]\"\"')
	userid = userid.to_i
	$timepull = follows1.values_at('followed_at')
	user = RestClient.get "https://api.twitch.tv/helix/users?id=#{userid}", {:'Client-ID' => configatron.clientid}
	
	$user1 = JSON.parse(user).fetch('data').first.each_with_index { |val| }
	$user1 = $user1.values_at('display_name')
	$user1 = $user1.to_s.delete('[{}]\"\"')
end

while true do
	timenew = RestClient.get "https://api.twitch.tv/helix/users/follows?to_id=#{configatron.chanid}", {:'Client-ID' => configatron.clientid} 
	$timenow = JSON.parse(timenew).fetch('data').first.each_with_index { |val| }.values_at('followed_at')
	if $timepull == $timenow
		puts 'sleeping'
		sleep 60
	else
		follower_new
		message = puts "Hey everyone #{$user1} Just followed the channel."
	end
end

