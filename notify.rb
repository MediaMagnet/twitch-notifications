#!/usr/bin/env ruby
require 'configatron'
require_relative 'config.rb'
require 'twitch-api'
require 'twitch/chat'
require 'rest-client'

apiclient = Twitch::Client.new access_token: configatron.clientid
chatclient = Twitch::Chat::Client.new(channel: configatron.channel, nickname: configatron.nick, password: configatron.oauth) do
  on(:connected) do
    send_message 'It works?'
  end

  on(:slow_mode) do
    send_message 'We walking folks'
  end



end

chatclient.run!
